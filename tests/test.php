<?php
//require_once 'vendor/autoload.php';
//require_once 'src/chimichanga/Chimichanga.php';
require_once __DIR__ . '/../vendor/autoload.php';
// Autoload files using Composer autoload

use Chimichanga\Chimichanga;

$wpWebsite = new Chimichanga();
$limit = 20;

// testing listPosts
$posts = $wpWebsite->listPosts(1, 20);
echo "@ Last $limit posts\n";
foreach ($posts as $post) {
    echo "PostID: ".$post->id."\t - ".$post->title->rendered."\n";
}

// testing get post
$singlePost = $wpWebsite->getPost($posts[0]->id);

echo "@ last Single Post: \n";
echo "PostID: ".$singlePost->id."\t - ".$singlePost->title->rendered."\n";
echo "Content: ".html_entity_decode(strip_tags($singlePost->content->rendered));


