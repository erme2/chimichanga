<?php
namespace Chimichanga;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\CurlResponse;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Chimichanga
{
    protected $wpWebsite = "";
    protected $client;

    function __construct(string $wpWebsite = 'https://demo.wp-api.org' )
    {
        $this->wpWebsite = "$wpWebsite/wp-json";
    }

    public function listPosts($page = 1, $limit = 10)
    {
        try {
            $return = $this->openEndPoint("/wp/v2/posts?page=$page&per_page=$limit");
        } catch (\Exception $exception) {
            throw $exception;
        }
        return $return;
    }

    public function getPost(int $postID)
    {
        try {
            $return = $this->openEndPoint("/wp/v2/posts/$postID");
        } catch (\Exception $exception) {
            throw $exception;
        }
        return $return;
    }

    private function openEndPoint(string $endPoint)
    {
        $client = HttpClient::create();
        try {
            $response = $client->request('GET', $this->wpWebsite.$endPoint);
        } catch (\Exception $exception) {
            throw $exception;
        }

        $statusCode = $response->getStatusCode();
        if($statusCode == 200) {
            return json_decode($response->getContent());
        } else {
            throw new \Exception("The WP website returned an error (http status code = $statusCode)");
        }
    }
}